This is a suite designed to help calculate and simulate the advantages of different motor setups

main.py will plot values (such as acceleration, torque, speed, etc) from a flat out acceleration run
torqueOptimize.py will calculate the gearing required to match the traction limits of the car
reverseTorque.py is for turning a force on the car to a torque at the motor
dragRace.py will visually simulate two cars racing in the 75m acceleration event.