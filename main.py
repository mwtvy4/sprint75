__author__ = 'mwt'
import time
from bokeh.plotting import figure , curdoc
import csv
from bokeh.client import push_session
from bokeh.models import ColumnDataSource

torqueFile = 'torque.csv'

lbs2kg = 1/(2.2)
ms2mph = 2.23694
carSpeed = 0
carDistance = 0


Tstep = .01
timePassed = 0
finishLine = 75#meters


#Car setup
mass = (519+170) * lbs2kg # in kg, artie is 519lbs + driver
tireRadius = .1999 #m
#tireRadiusft = .6561 #ft
sprocket = 1.5 #ratio of rear gear to sprocket
totalGearing = sprocket / tireRadius
#traction limits
weightTransfer = 1.1
normalForce = (mass/2) * weightTransfer * 9.81 #normal force on rear with 10% weight shift + 9.8 for G
cFric = 2.5 # coefficient of friction
tracLimit = normalForce * cFric #maximum longitudinal force


print("trac limit is ", tracLimit, "N")
# print out the initial torque
with open(torqueFile) as csvfile:
    firstreader = csv.DictReader(csvfile, fieldnames=['rpm','torque'])
    for row in firstreader:
        firstTorque = float(row['torque'])
        break
print(firstTorque * totalGearing)


#Bokeh setup
p = figure()
session = push_session(curdoc())
dataSource = dict(x=[0],y=[0])
CDS = ColumnDataSource(dataSource)
r1 = p.line(x='x',y='y',source=CDS)


def update():
    global carSpeed
    global carDistance
    global timePassed
    timePassed += Tstep #update time


    carDistance += carSpeed * Tstep
    carSpeed += ((carForce(carSpeed, tracLimit, sprocket, tireRadius) * Tstep) * ms2mph) / mass  ## mph from m/s

    CDS.stream(dict(x=[timePassed],y=[carSpeed]))#update the CDS

    if carDistance > finishLine: ## stop after you cross the finish line
        print(timePassed)
        time.sleep(15)# let the plot catch up
        #session.close(why="you win!")
        exit()


def carForce(mph, tracLimit, sprocket, tireRadius):
    # use CSV of tq vs rpm
    #convert speed to rpm at wheel
    ftsec = mph * 1.46666
    m2ft = .6561/.199
    wheelRPM = ftsec * 60 / (2 * tireRadius * m2ft * 3.1415)
    motorRPM = wheelRPM * sprocket
    lowT = 0
    highT = 0
    lowrpm = 0
    highrpm = 0
    with open(torqueFile) as csvfile:
        reader = csv.DictReader(csvfile, fieldnames=['rpm','torque'])
        for row in reader:
            #global lowT
            #global highT

            lowT = highT
            lowrpm = highrpm
            highT = float(row['torque'])
            highrpm = float(row['rpm'])
            rpmstep = highrpm-lowrpm
            #print(motorRPM)
            if (float(row['rpm']) > motorRPM):
                #print("break!")
                break
    motorTorque = (lowT * (rpmstep - (motorRPM - lowrpm))  + highT * (rpmstep - (highrpm - motorRPM))) / rpmstep
    output = motorTorque * sprocket / tireRadius
    if (output > tracLimit):
        #print(output)
        output = tracLimit
        #print("BURNOUT!")
    return output
    # loop through csv until rpm is higher than reading rpm
    # use that reading and the previous to blend
    # # smooth between values


curdoc().add_periodic_callback(update,Tstep*1000)
session.show(p)
time.sleep(2)
session.loop_until_closed()