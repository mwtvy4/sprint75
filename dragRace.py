__author__ = 'mwt'
import time
from bokeh.plotting import figure , curdoc
import csv
from bokeh.client import push_session
from bokeh.models import ColumnDataSource
kg2lbs = 2.2
ms2mph = 2.23694
Tstep = .01
timePassed = 0

##Car1 setup
car1file = 'torqueActual.csv'
mass = (519+170)/kg2lbs # in kg, artie is 519lbs + driver
carSpeed = 0
carDistance = 0
#tireRadius = .6561 #ft
tireRadius = .199 #m
sprocket = 3.5 #ratio of rear gear to sprocket
totalGearing = sprocket / tireRadius
weightTransfer = 1.1
normalForce = (mass/2) * weightTransfer * 9.8 #normal force on rear with 10% weight shift + 9.8 for G
cFric = 2.5 # coefficient of friction
tracLimit = normalForce * cFric #maximum longitudinal force
ET = 0 # timeslip

#Car2
car2file = 'torqueDual208.csv'
mass2 = (619+170)/kg2lbs # in kg, artie is 519lbs + driver
carSpeed2 = 0
carDistance2 = 0
#tireRadius2 = .6561 #ft
tireRadius2  = .199 #m
sprocket2 = 2.5 #ratio of rear gear to sprocket
totalGearing2 = sprocket / tireRadius2
weightTransfer2 = 1.1
normalForce2 = (mass2/2) * weightTransfer2 * 9.8 #normal force on rear with 10% weight shift + 9.8 for G
cFric2 = 2.5 # coefficient of friction
tracLimit2 = normalForce2 * cFric2 #maximum longitudinal force
ET2 = 0


p = figure()
session = push_session(curdoc())

#dataSource = dict(x=[0],y=[0])
CDS = ColumnDataSource(dict(x=[10],y=[0]))
CDS2 = ColumnDataSource(dict(x=[20], y=[0]))


finish = 75#meters

r1 = p.line(x='x',y='y',source=CDS)
r2 = p.line(x='x',y='y' ,source=CDS2)
finishLine = p.line([5,25],[finish,finish])
# v = v_0 + a*Tstep??
# f = ma   a = f/m
def update():
    global carSpeed
    global carSpeed2
    global carDistance
    global carDistance2
    global timePassed
    global ET
    global ET2

    timePassed += Tstep #update time
    #carForce =  # N
    carDistance += carSpeed * Tstep
    carDistance2 += carSpeed2 * Tstep
    carSpeed += ((carForce(carSpeed, car1file, tracLimit ,sprocket,tireRadius) * Tstep) * ms2mph) / mass  ## mph from m/s
    carSpeed2 += ((carForce(carSpeed2,car2file,tracLimit2 ,sprocket2,tireRadius2)* Tstep)* ms2mph) / mass2
    CDS.stream(dict(x=[10],y=[carDistance]))
    CDS2.stream(dict(x=[20],y=[carDistance2]))
    if (carDistance < finish):
        ET = timePassed
        trap = carSpeed
    if (carDistance2 < finish):
        ET2 = timePassed
        trap2 = carSpeed2
    if ((carDistance > finish) & (carDistance2 > finish)):
        print("Car 1: ", ET , "sec @ " , carSpeed)
        print("Car 2: ", ET2 , "sec @ " , carSpeed2)
        #session.close(why="time's up")
        exit()


def carForce(mph, torqueFile , tracLimit, sprocket, tireRadius): # v in mph
    # use CSV of tq vs rpm
    #convert speed to rpm at wheel
    ftsec = mph * 1.46666
    wheelRPM = ftsec * 60 / (2 * tireRadius * 3.1415)
    motorRPM = wheelRPM * sprocket

    lowT = 0
    highT = 0
    lowrpm = 0
    highrpm = 0

    with open(torqueFile) as csvfile:
        reader = csv.DictReader(csvfile, fieldnames=['rpm','torque'])
        for row in reader:
            lowT = highT
            lowrpm = highrpm
            highT = float(row['torque'])
            highrpm = float(row['rpm'])
            rpmstep = highrpm - lowrpm
            #print(motorRPM)
            if (float(row['rpm']) > motorRPM):
                #print("break!")
                break

    motorTorque = (lowT * (rpmstep - (motorRPM - lowrpm))  + highT * (rpmstep - (highrpm - motorRPM))) / rpmstep
    output = motorTorque * sprocket / tireRadius
    if (output > tracLimit):
        print(output)
        output = tracLimit
        print("BURNOUT!")
    return output
    # loop through csv until rpm is higher than reading rpm
    # use that reading and the previous to blend
    # # smooth between values


curdoc().add_periodic_callback(update,Tstep*1000)
session.show(p)
time.sleep(5)
session.loop_until_closed()