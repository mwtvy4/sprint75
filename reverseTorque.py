__author__ = 'mwt'
wheelRadius = .199
sprocket = 3.5


dragOnCar = float(input("what's the drag in lbs?"))
dragNewton = dragOnCar * 4.45


#convert to torque at wheels
wheelTorque = dragNewton * wheelRadius

#torque at motor
motorTorque = wheelTorque / sprocket

print(motorTorque)