__author__ = 'mwt'
import csv

torqueFile = 'torqueDual208.csv'

kg2lbs = 2.2
mass = (519+170)/kg2lbs # in kg, artie is 519lbs + driver
#tireRadius = .6561 #ft
tireRadius = .199 #m
sprocket = 4 #ratio of rear gear to sprocket
totalGearing = sprocket / tireRadius
#traction limits
weightTransfer = 1.1
normalForce = (mass/2) * weightTransfer * 9.8 #normal force on rear with 10% weight shift + 9.8 for G
cFric = 2.5 # coefficient of friction
tracLimit = normalForce * cFric #maximum longitudinal force

print("trac limit is ", tracLimit, "N")
# print out the initial torque
with open(torqueFile) as csvfile:
    firstreader = csv.DictReader(csvfile, fieldnames=['rpm','torque'])
    for row in firstreader:
        firstTorque = float(row['torque'])
        break

print("Optimal gear ratio is:", (tracLimit / firstTorque) * tireRadius , " : 1")